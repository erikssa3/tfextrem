Rails.application.routes.draw do

  get '/about', to: "static_pages#about"

  get "/contact", to: "static_pages#contact"

  get "/instructions", to: "static_pages#instructions"

  get "/rules", to: "static_pages#rules"
  
  get '/registration', to: "teams#new"
  
  get '/teams', to: "teams#index"
  
  root "static_pages#home"
  
  resources :teams 

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
