class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :member1
      t.string :member2
      t.string :member3
      t.string :email
      t.string :class

      t.timestamps
    end
  end
end
