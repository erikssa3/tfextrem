class ChangeNameAgain < ActiveRecord::Migration[5.0]
  def change
    rename_column :teams, :type, :race_type
  end
end
