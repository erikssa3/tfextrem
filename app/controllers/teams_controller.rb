class TeamsController < ApplicationController
  before_filter :authenticate, only: [:index]

  def authenticate
    authenticate_or_request_with_http_basic('Administration') do |username, password|
      username == 'admin' && password == 'password'
    end
  end
  
  def new
    @team = Team.new 
  end
  
  def destroy
    Team.find(params[:id]).destroy
    flash[:success] = "Team deleted"
    redirect_to teams_url
  end
  
  def create
    @team = Team.new(team_params)
    
    if @team.save
      TeamMailer.registration_confirmation(@team).deliver_now
      flash[:success] = "Thank you for registering! A confirmation email has also been sent."
      redirect_to root_path 
    else
      flash[:danger] = "Please, make sure that the form is filled in properly."
      render 'new'
    end
  end
  
  def index 
    @teams = Team.all
  end
  
  
  private
    def team_params
      params.require(:team).permit(:name, :member1, :member2, :member3, :email, :race_type)
    end
  
end
