class StaticPagesController < ApplicationController
  
  def home
  end

  def about
  end

  def contact
  end

  def instructions
  end

  def rules
  end

  def results
  end

end
