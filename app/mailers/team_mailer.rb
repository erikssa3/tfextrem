class TeamMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.team_mailer.registration_confirmation.subject
  #
  
  
  def registration_confirmation(team)
    @team = team
    mail to: team.email, subject: "TF-xtreme confirmation"
  end
end
