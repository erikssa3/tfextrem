class ApplicationMailer < ActionMailer::Base
  default from: 'noreplay@tfxtreme.com'
  layout 'mailer'
end
