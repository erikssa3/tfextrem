class Team < ApplicationRecord
  validates :name,      presence: true 
  validates :member1,   presence: true
  validates :member2,   presence: true
  validates :member3,   presence: true
  validates :email,     presence: true
end
