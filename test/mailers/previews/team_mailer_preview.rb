# Preview all emails at http://localhost:3000/rails/mailers/team_mailer
class TeamMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/team_mailer/registration_confirmation
  def registration_confirmation
    team = Team.first
    TeamMailer.registration_confirmation(team)
  end

end
